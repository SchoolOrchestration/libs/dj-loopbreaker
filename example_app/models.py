from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from loopbreaker.decorators import throttle


class Todo(models.Model):

    text = models.CharField(blank=True, null=True, max_length=255)
    done = models.BooleanField(default=False)

    created_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)

@receiver(post_save, sender=Todo, dispatch_uid="example.say_hi")
@throttle(signal_id="foo.bar", expire_after=10)
def say_hi(instance, created, **kwargs):
    # normally this would cause an infinte loop
    instance.done = not instance.done
    instance.save()
