FROM python:3.6
ENV PYTHONUNBUFFERED 1
RUN mkdir -p /code
WORKDIR /code
COPY requirements* /code/
RUN pip install -U pip
RUN pip install -r requirements.txt && pip install -r requirements-dev.txt
COPY . /code/